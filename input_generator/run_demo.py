from input_generator.binary_input_gen import BinaryInputGenerator
from input_generator.random_input import RIG

def run_demo():
    bin_ip_gen = BinaryInputGenerator()
    bin_ip_gen.generate_bin_pattern()
    bin_ip_gen.write_to_file()
    print(bin_ip_gen)

def rand_run_demo():
    ip = RIG()
    ip.create_random_pattern_list()
    ip.write_to_fil()


if __name__ == '__main__':
    #run_demo()
    rand_run_demo()