import numpy as np
from itertools import chain
import torch
from scipy import linalg
import pickle
import gzip
from pkg_resources import resource_filename
import sys
import csv
import os
from config import EC2
from config import HopfieldNetworkConfig as hnconfig


class RIG():

    def __init__(self):
        self.pattern_length = EC2.PATTERN_LENGTH
        self.pattern_width = EC2.PATTERN_WIDTH
        self.no_of_patterns = hnconfig.NO_OF_PATTERN
        self.on_prob = EC2.ACTIVATION_RATIO
        self.pattern_list = []
        self.nr_neurons = EC2.NO_OF_NEURONS

    def create_random_pattern_ori(self):

        p = np.random.binomial(1, self.on_prob, self.pattern_length * self.pattern_width)
        #p = p * 2 - 1  # map {0, 1} to {-1 +1}

        return p.reshape((self.pattern_length, self.pattern_width))

    def create_random_pattern(self):

        #p = torch.Tensor(self.nr_neurons)
        #print(p)
        #p = torch.distributions.Binomial(total_count= self.nr_neurons, probs = self.on_prob)
        p = np.random.binomial(1, self.on_prob, self.pattern_length * self.pattern_width)
        q = torch.from_numpy(p)
        #print ("torch tensor")
        #print (q)
        #return q.view(self.pattern_length, self.pattern_width)
        return q

    def create_random_pattern_list(self):

        #p = list()
        for i in range(self.no_of_patterns):
            self.pattern_list.append(self.create_random_pattern())
        print("pattern list")
        print(self.pattern_list)
        #return p


    def write_to_fil(self):
        file_name = "./saved_inputs/randpat.txt"
        file_object = open(file_name, mode='w')
        csv_writer = csv.writer(file_object)
        curr_list = []
        curr_tensor_list = torch.stack(self.pattern_list, dim=1)
        curr_tensor_np = curr_tensor_list.numpy()
        curr_tensor_np = np.transpose(curr_tensor_np, (1,0))
        np.savetxt(fname=file_name, X=curr_tensor_np, fmt="%d")
        file_object.close()

    def read_from_fil(self):
        file_name = os.path.join(os.path.dirname(os.path.realpath(__file__)), './saved_inputs/randpat.txt')
        pattern_list = np.loadtxt(fname=file_name, dtype=int)
        pattern_list[pattern_list == 0] = -1
        for i in range(pattern_list.shape[0]):
            pl = np.transpose(pattern_list[i, :])
            self.pattern_list.append(pl)
        print("inside read")
        print(self.pattern_list)
        return self

    if __name__ == '__main__':
        ip = RIG()
        ip.create_random_pattern_list()
        ip.write_to_fil()
        #ip.read_from_fil()
