import numpy as np
import torch
from dendrate_gyrus.config import Config


def create_activation_list(size: int, activation_ratio: float):
    value_list = torch.FloatTensor(size).uniform_() > activation_ratio
    return value_list.int()


def populate_weights_uniform(length: int, width: int):
    return torch.FloatTensor(length, width).uniform_()


def input_set():
    input_list = [1, 1, 1, 0, 0, 1, 0, 0, 1, 0]
    return torch.FloatTensor(input_list), len(input_list)


def populate_weights(length: int, width: int):
    return torch.rand(length, width)


def create_zeroes(x_dim, y_dim):
    return torch.zeros(x_dim, y_dim, dtype=torch.float32)


def create_op_zeroes(dim):
    return torch.zeros(dim, dtype=torch.long)


def ReLu(val):
    if val > 0:
        return val

    else:
        return 0


def calc_sum(input_array):
    return torch.sum(input_array)


def calc_max(input_array):
    return torch.max(input_array)


def calc_mean(input_array):
    return torch.mean(input_array).float()


def calc_sum_of_two_tensors(input_array_1, input_array_2):
    return torch.sum(input_array_1, input_array_2)


if __name__ == '__main__':
    val_list = create_activation_list(10)
    print(val_list)
    print(len(val_list))
