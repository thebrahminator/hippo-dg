from hopfield_network import pattern_tools, plot_tools, network
from config import HopfieldNetworkConfig as hnconfig
from config import Chemicals as chem
from config import EC2
from ec2.ec2 import EntorinhalCortex
from ojas_rule.run_oj import run_oja_from_dg, run_oja_from_ec2
from ojas_rule.oj import OjasRule
from dendrate_gyrus.run_dg import run_demo, run_demo_modified
import numpy
import time
import numpy as np
import torch
from typing import List, Union
from utils import calc_sum_of_two_tensors
from input_generator.binary_input_gen import BinaryInputGenerator
from input_generator.random_input import RIG
#TODO: Change the name of file to training.py after finishing the code

#TYPE DEFINITIONS
two_dimesional_vector = np.ndarray


def initialise_hopfield_network()-> network.HopfieldNetwork:

    hopfield_net = network.HopfieldNetwork(nr_neurons=hnconfig.NO_OF_NEURONS)
    return hopfield_net

def pattern_select(parameter: int) -> List:

    if parameter == 1:
        pattern_list = BinaryInputGenerator().read_from_file().pattern_list
        new_pattern_list = pattern_list

        return new_pattern_list

    if parameter == 2:
        pattern_list = RIG().read_from_fil().pattern_list
        new_pattern_list = pattern_list

        return new_pattern_list

def plot_graphs(pattern_list: List):

    #plot_tools.plot_pattern_list(pattern_list=pattern_list)
    overlap_matrix = pattern_tools.compute_overlap_matrix(pattern_list=pattern_list)
    plot_tools.plot_overlap_matrix(overlap_matrix=overlap_matrix)

    print(f"Average Overlap: {numpy.average(overlap_matrix)}")

    print(f"No of patterns = {hnconfig.NO_OF_PATTERN}, No of neurons = {hnconfig.NO_OF_NEURONS}")



def oplayer_dict(activation_list: List):
    oplayer_list = []
    for idx, value in enumerate(activation_list):
        temp_dict = dict()
        temp_dict['idx'] = idx
        temp_dict['value'] = value.item()
        oplayer_list.append(temp_dict)
    return oplayer_list


def Test_EC2_EC3(pattern_list: List, ojas_obj: Union[None, OjasRule] = None):
    converted_pattern = torch.tensor(pattern_list)

    if not ojas_obj:
        ojas_rule: OjasRule = run_oja_from_ec2(input_layer=converted_pattern)
    else:
        ojas_rule: OjasRule = ojas_obj

    print("test done")
    return ojas_rule.oplayer, ojas_rule

def process_pattern_from_EC2_to_CA3(pattern_list: List, ojas_obj: Union[None, OjasRule] = None):
    # pattern_list = np.array(pattern_list)
    #print(pattern_list)
    converted_pattern = torch.tensor(pattern_list)

    if not ojas_obj:
        ojas_rule: OjasRule = run_oja_from_ec2(input_layer=converted_pattern)
    else:
        ojas_rule: OjasRule = ojas_obj

    ojas_rule.weight_update()
    ojas_rule.weight_normalisation()
    print ("Pattern done")
    return ojas_rule.oplayer, ojas_rule


def process_pattern_from_EC2_to_DG(pattern_list: np.ndarray, ojas_obj: Union[None, OjasRule] = None):
    flattened_list = pattern_list.flatten()
    oplayer_list, ojas_obj = run_demo_modified(pattern_=flattened_list, flag=0, ojas_rule=ojas_obj)
    return oplayer_list, ojas_obj


def process_pattern_from_DG_to_CA3(activation_from_DG: List):
    pass


def extract_activation_list(activation_ratio: List):
    output_dict = oplayer_dict(activation_list=activation_ratio)
    sorted_oplayer = sorted(output_dict, key=lambda k: k['value'], reverse=True)
    no_of_elem = int(hnconfig.OUTPUT_ACTIVATION_RATIO * len(activation_ratio))
    sorted_oplayer = sorted_oplayer[:no_of_elem]
    activated_indices = []

    for data in sorted_oplayer:
        activated_indices.append(data['idx'])

    for idx, value in enumerate(activation_ratio):
        if idx in activated_indices:
            activation_ratio[idx] = 1
        else:
            activation_ratio[idx] = -1

    return activation_ratio


def network_recall(input_pattern: List, pattern_tools, no_of_flips: int):

    noisy_init_state = pattern_tools.flip_n(input_pattern[0], no_of_flips)
    entorinhal_cortex = EntorinhalCortex([noisy_init_state])

    ojas_object_from_ca3 = None
    op_from_ec2_to_ca3, ojas_object_from_ca3 = Test_EC2_EC3(
        pattern_list=entorinhal_cortex.pattern_list[0], ojas_obj=ojas_object_from_ca3)

    return op_from_ec2_to_ca3

def run_program() -> List[two_dimesional_vector]:


    #Setting up CA3 - Hopfield
    hopfield_net = initialise_hopfield_network()
    pattern_factory = pattern_tools.PatternFactory(hnconfig.PATTERN_FACTORY_LENGTH, hnconfig.PATTERN_FACTORY_WIDTH)

    pattern_list = pattern_select(parameter=2)
    plot_graphs(pattern_list=pattern_list)

    entorinhal_cortex = EntorinhalCortex(pattern_list)

    ojas_object_from_ca3 = None
    ojas_object_from_dg = None

    activated_patterns: List = list()
    for pattern_ec, pattern_naive in zip(entorinhal_cortex.pattern_list, pattern_list):

        op_from_ec2_to_ca3, ojas_object_from_ca3 = process_pattern_from_EC2_to_CA3(pattern_list=pattern_ec,
                                                                                   ojas_obj=ojas_object_from_ca3)
        op_from_ec2_to_dg, ojas_object_from_dg = process_pattern_from_EC2_to_DG(pattern_list=pattern_naive,
                                                                                ojas_obj=ojas_object_from_dg)
        # print("OP from EC2: ", op_from_ec2_to_ca3)
        # print("OP from DG: ", op_from_ec2_to_dg)
        #print("Size from DG: ", op_from_ec2_to_dg.size())
        #print("Size from EC2: ", op_from_ec2_to_ca3.size())
        combined_value = chem.DOPAMINE * op_from_ec2_to_dg + op_from_ec2_to_ca3
        activated_list = extract_activation_list(activation_ratio=combined_value)
        ojas_object_from_dg.oplayer = activated_list
        ojas_object_from_ca3.oplayer = activated_list
        activated_patterns.append(activated_list.numpy())
        # print("Size of Combined Value: ", combined_value.size())
        # print("Comb Val values: ", combined_value)

    # modified_list = hopfield_net.store_patterns_with_dg(activated_patterns)
    # print(modified_list)
    # modified_oja_patterns: List = []
    # for pattern in modified_list:
    #     oja_output: OjasRule = run_oja_from_dg(input_layer=pattern)
    #     modified_oja_patterns.append(oja_output.activation_list_ec_ca3)

    #activated_patterns = convert_list_from_0_1(pattern_list=activated_patterns)
    hopfield_net.store_patterns(pattern_list=activated_patterns)

    # plot_tools.plot_nework_weights(hopfield_network=hopfield_net)

    modified_list = activated_patterns
    del activated_patterns
    no_of_flips = int (EC2.ERROR_RATE * EC2.NO_OF_NEURONS / 100)
    print ("no of flips = {0}".format(no_of_flips))
    recall_value = network_recall(input_pattern=pattern_list, no_of_flips = no_of_flips, pattern_tools=pattern_tools)
    recall_activation_CA3 = extract_activation_list(activation_ratio=recall_value)

    # for i in range(1,2):
    #     for j in range(1,2):
    #         noisy_init_state = recall_value.numpy()
    #         # plot_tools.plot_pattern_list([pattern_list[0], noisy_init_state])
    #         hopfield_net.set_state_from_pattern(noisy_init_state)
    #         hopfield_net.set_dynamics_sign_async()
    #         start_time = time.time()
    #         states = hopfield_net.run_with_monitoring(nr_steps=4)
    #         time_taken = time.time() - start_time
    #         print(f"Run {i} with {j} flips took {time_taken} steps")
    #         states_as_patterns = pattern_factory.reshape_patterns(states)
    #         modified_as_patterns = pattern_factory.reshape_patterns(modified_list)
    #         print(states_as_patterns[0].shape, modified_as_patterns[0].shape)
    #         plot_tools.plot_pattern_list([states_as_patterns[0], modified_as_patterns[0]])
    #         title = f"Testing with {j} flips and {hnconfig.NO_OF_PATTERN} patterns with {hnconfig.NO_OF_NEURONS}"
    #         plot_tools.plot_state_sequence_and_overlap(states_as_patterns, modified_as_patterns,
    #                                                     reference_idx=0, suptitle=title)

    hopfield_net.set_state_from_pattern(recall_activation_CA3.numpy())
    hopfield_net.set_dynamics_sign_async()
    states = hopfield_net.run_with_monitoring(nr_steps=4)
    states_as_patterns = pattern_factory.reshape_patterns(states)
    modified_as_patterns = pattern_factory.reshape_patterns(modified_list)
    plot_tools.plot_pattern_list([states_as_patterns[0], modified_as_patterns[0]])
    title = f"Testing with {no_of_flips} flips and {hnconfig.NO_OF_PATTERN} patterns with {hnconfig.NO_OF_NEURONS} neurons"
    plot_tools.plot_state_sequence_and_overlap(states_as_patterns, modified_as_patterns,
                                               reference_idx=0, suptitle=title)

    return 1

if __name__ == '__main__':
    run_program()


