from ojas_rule.run_oj import run_oja_from_ec2
from ojas_rule.oj import OjasRule
import torch

class EntorinhalCortex(object):

    def __init__(self, pattern_list):
        self.pattern_list = pattern_list

    def process_pattern_one_by_one(self):

        activated_list = []
        for pattern in self.pattern_list:
            converted_pattern = torch.tensor(pattern[0])
            oja_obj_from_ec2: OjasRule = run_oja_from_ec2(input_layer=converted_pattern)
            self.pass_to_CA3(oja_obj_from_ec2.activation_list_ec_ca3)
            oja_obj_from_ec2.weight_update()
            #oja_obj_from_ec2.matrix_dilution()
            oja_obj_from_ec2.weight_normalisation()

    @staticmethod
    def pass_to_DG(pattern: torch.FloatTensor):
        #TODO: Pass the code via Oja to CA3 again
        pass

    @staticmethod
    def pass_to_CA3(pattern: torch.FloatTensor):
        pass