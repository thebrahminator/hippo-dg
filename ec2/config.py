class Config(object):
    APP_NAME: str = "Dendrate Gyrus -- Hippo"
    SECRET_KEY: str = "Locats"
    CODER_NAME: str = "Vishwanath"

class OjasConfig(Config):

    ALPHA = 0.25

    OUTPUT_LAYER_SIZE = 100
    OUTPUT_ACTIVATION_RATIO = 0.2
    DILUTION = 0.5