import torch, random
from utils import create_activation_list, calc_max, ReLu, \
    calc_sum, calc_mean, create_op_zeroes, populate_weights_uniform, input_set
from config import DentateGyrusConfig as dg_config

class DendrateGyrus():

    def     __init__(self, pattern_list, flag=1):

        self.dg_config = dg_config
        self.dilution = self.dg_config.DILUTION

        if flag == 0:
            self.iplayer_size = len(pattern_list)
            self.iplayer = torch.from_numpy(pattern_list)
            self.iplayer.float()

        else:
            if self.dg_config.STANDARD_INPUT == 0:
                self.iplayer, self.iplayer_size  = input_set()

            else:
                self.iplayer_size = self.dg_config.INPUT_LAYER_NEURONS
                self.iplayer_dilation = self.dg_config.INPUT_DILATION
                self.iplayer = create_activation_list(size=self.iplayer_size,
                                              activation_ratio=self.iplayer_dilation)

        self.oplayer_size = self.dg_config.OUTPUT_LAYER_NEURONS
        self.oplayer = create_op_zeroes(self.oplayer_size)
        self.weight_matrix = populate_weights_uniform(self.iplayer_size, self.oplayer_size)
        self.matrix_dilution_init()
        self.weight_normalisation()

        self.alpha = self.dg_config.ALPHA
        self.beta = self.dg_config.BETA

    def matrix_dilution_init(self):
        nr_of_values = self.iplayer_size * self.oplayer_size
        no_of_zeros = int((1 - self.dilution) * nr_of_values)

        random.seed(3)
        x = random.sample(range(0, nr_of_values), no_of_zeros)
        weight_matrix = self.weight_matrix.numpy()
        for i in x:
            weight_matrix.flat[i] = 0
        self.weight_matrix = torch.from_numpy(weight_matrix)

    def weight_normalisation(self):
        for j in range(0, self.oplayer_size):
            jth_weight = self.j_th_weight(j)
            updated_weights = self.weight_normalisation_jth(j_thweights=jth_weight)
            self.weight_matrix[:, j] = updated_weights

    def weight_normalisation_jth(self, j_thweights):
        sum_ = calc_sum(j_thweights)
        j_thweights /= sum_
        return j_thweights

    def j_th_weight(self, j):
        j_th_column = self.weight_matrix[:, j]
        return j_th_column

    def i_th_weight(self, i):
        i_th_row = self.weight_matrix[i, :]
        return i_th_row

    @staticmethod
    def extract_max(input_array):
        full_list = []
        for val in input_array:
            full_list.append(val.item())

        return max(full_list)

    def check_if_computation(self, jth_weight):
        sum_ = 0
        for i in range(0, self.iplayer_size):
            #print(jth_weight[i])
            #print(self.iplayer[i])
            sum_ += jth_weight[i].item() * self.iplayer[i].item()

        if sum_ > 0.1:
            return True
        else:
            return False

    def inner_value(self, i):
        result_list = []
        for k in range(0, self.oplayer_size):

            curr_weight_row = self.j_th_weight(k)
            max_weight = calc_max(curr_weight_row)
            #print(max_weight.item)
            temp_curr_value = ( self.weight_matrix[i, k] / max_weight)
            max_op = calc_max(self.oplayer).item()
            if max_op == 0:
                max_op = 1
            curr_2 = (self.oplayer[k] / max_op).item()
            #print(curr_2)
            curr_value = temp_curr_value * curr_2
            result_list.append(curr_value)

        return self.extract_max(input_array=result_list)

    def calc_y_j(self, jth_weight):
        result_list = []
        for i in range(0, self.iplayer_size):
            temp_curr_value = self.alpha * self.inner_value(i)
            curr_value = ReLu(1 - temp_curr_value)
            #print(curr_value)

            interim_result = jth_weight[i].item() * self.iplayer[i].item() * curr_value
            result_list.append(interim_result)

        #print(result_list)
        result_list = torch.FloatTensor(result_list)
        #print(result_list)
        return calc_sum(result_list)


    def weight_change(self):
        ip_sum = calc_sum(self.iplayer)
        op_sum = calc_sum(self.oplayer)

        ip_mean = calc_mean(self.iplayer.float())
        op_mean = calc_mean(self.oplayer)

        for i in range(0, self.iplayer_size):
            left_val = self.beta * ( (self.iplayer[i].float() - ip_mean.float() ) / ip_sum.float())
            for j in range(0, self.oplayer_size):
                if (self.weight_matrix[i, j] == 0):
                    pass
                    #self.weight_matrix[i, j] = 0
                else:
                    right_val = ReLu( self.oplayer[j] - op_mean) / op_sum
                    delta_change = left_val * right_val
                    self.weight_matrix[i,j] += delta_change

        self.weight_normalisation()

    def run_demo(self):
        result_list = []

        for j in range(0, self.oplayer_size):
            jth_weight = self.j_th_weight(j)
            if_compute = self.check_if_computation(jth_weight=jth_weight)
            if if_compute:
                result_list.append(self.calc_y_j(jth_weight=jth_weight))
            else:
                result_list.append(torch.FloatTensor([0]))

        interim_list = []
        for result in result_list:
            interim_list.append(result.item())

        self.oplayer = torch.FloatTensor(interim_list)
        self.weight_change()

    @property
    def oplayer_dict(self):
        oplayer_list = []
        for idx, value in enumerate(self.oplayer):
            temp_dict = dict()
            temp_dict['idx'] = idx
            temp_dict['value'] = value.item()
            oplayer_list.append(temp_dict)
        return oplayer_list

    def extract_activation_list(self):

        sorted_oplayer = sorted(self.oplayer_dict, key=lambda k: k['value'], reverse=True)
        no_of_elem = int(self.dg_config.OUTPUT_ACTIVATION_RATIO * self.oplayer_size)
        sorted_oplayer = sorted_oplayer[:no_of_elem]
        activated_indices = []

        for data in sorted_oplayer:
            activated_indices.append(data['idx'])

        for idx, value in enumerate(self.oplayer):
            if idx in activated_indices:
                self.oplayer[idx] = 1
            else:
                self.oplayer[idx] = 0


if __name__ == '__main__':
    #RUNNING SHIFTED TO dg_run.py
    pass



