from typing import List


class Config(object):
    APP_NAME: str = "Dendrate Gyrus -- Hippo"
    SECRET_KEY: str = "Locats"
    CODER_NAME: str = "Vishwanath"


class TestConfig(Config):
    INPUT_LAYER_NEURONS = 100
    INPUT_DILATION = 0.5  # Activation within IP Layer of DG

    STANDARD_INPUT = 0

    OUTPUT_LAYER_NEURONS = 1500
    OUTPUT_DILATION = 0.6  # NOT BEING USED
    OUTPUT_ACTIVATION_RATIO = 0.15

    ALPHA = 0.25
    BETA = 0.25


class MainConfig(Config):

    INPUT_LAYER_NEURONS = 5
    INPUT_DILATION = 0.5

    STANDARD_INPUT = 0

    OUTPUT_LAYER_NEURONS = 1
    OUTPUT_DILATION = 0.6
    OUTPUT_ACTIVATION_RATIO = 0.15

    ALPHA = 0.25
    BETA = 0.25



class OjasConfig(Config):
    ALPHA = 0.25

    OUTPUT_LAYER_SIZE = 100
    OUTPUT_ACTIVATION_RATIO = 0.2
