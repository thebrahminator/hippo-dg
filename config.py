import arrow


class GlobalConfig(object):

    APP_NAME = "Vidya"
    RUN_DATE = str(arrow.get())
    RUN_DESCRIPTION = "First run"


class EC2(object):

    NO_OF_NEURONS = 100
    PATTERN_LENGTH = 10
    PATTERN_WIDTH = 10
    ACTIVATION_RATIO = 0.5
    ERROR_RATE = 10


class HopfieldNetworkConfig(GlobalConfig):

    NO_OF_PATTERN = 8
    NO_OF_NEURONS = 200
    DILUTION = 0.2
    ON_PROBABLITY = 0.4

    PATTERN_FACTORY_LENGTH = 20
    PATTERN_FACTORY_WIDTH = 10

    OUTPUT_ACTIVATION_RATIO = 0.15


class DentateGyrusConfig(GlobalConfig):

    INPUT_LAYER_NEURONS = None
    INPUT_DILATION = 0.5  # Activation within IP Layer of DG

    DILUTION = 0.6
    STANDARD_INPUT = 0

    OUTPUT_LAYER_NEURONS = 500

    OUTPUT_ACTIVATION_RATIO = 0.15

    ALPHA = 0.25
    BETA = 0.25

class OjasConfig(GlobalConfig):
    OUTPUT_LAYER_NEURONS = 200
    DILUTION_DG_CA3 = 0.6
    ELIMINATION = 1 # SET TO 1 TO ENABLE
    OUTPUT_ACTIVATION_RATIO = 0.15

    BETA = 0.25
    ALPHA = 0.6

class OjasConfigEC2(GlobalConfig):
    OUTPUT_LAYER_NEURONS = 200
    DILUTION_EC2_CA3 = 0.6
    ELIMINATION = 1
    OUTPUT_ACTIVATION_RATIO = 0.15

    BETA = 0.25
    ALPHA = 0.6

class Chemicals(object):

    DOPAMINE = 1.0