from ojas_rule.oj import OjasRule
import torch
from typing import List


def run_oja_from_dg(input_layer):
    ojr = OjasRule(input_pattern=input_layer, source_of_entry=1)

    for i in range(0, 2):  # TODO: Change the value to a more plausible run sequence
        ojr.activation_function()
        ojr.weight_update()
        #ojr.matrix_dilution()
        ojr.weight_normalisation()

    ojr.extract_activation_list()
    return ojr.oplayer


def run_oja_from_ec2(input_layer: List) -> OjasRule:
    ojr = OjasRule(input_pattern=input_layer, source_of_entry=2)

    for i in range(0, 3):
        ojr.activation_function()

    return ojr


if __name__ == '__main__':
    ip_layer = [1, 0, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1]
    ip_layer_float = torch.tensor(ip_layer)
    print(run_oja_from_dg(input_layer=ip_layer_float))
